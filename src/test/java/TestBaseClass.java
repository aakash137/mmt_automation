import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestBaseClass {
    BaseClass baseClass;

    @BeforeClass
    public void initialize(){
        baseClass = new BaseClass();

    }

    @Test(priority = 1)
    public void testSetUp(){
        baseClass.setupApplication();
    }

    @Test(priority = 2)
    public void testJourneyDetails(){
        FirstFlightDetails flightDetails = baseClass.journeyDetails("Bangalore (BLR)", "Kolkata (CCU)");

        System.out.println(flightDetails.getSrcFlight()+"  "+flightDetails.getDepTimeTerminal()+"  "+flightDetails.getArrTimeTerminal()+"\n"+flightDetails.getRetrnFlight()+"  "+flightDetails.getReturndDeptTimeTerminal()+"  "+flightDetails.getRetrnArrTimeTerminal()+"  "+flightDetails.getPrice());
    }

    @Test(priority = 3)
    public void testFlightResults(){

        Assert.assertTrue(baseClass.getFlightResults());
    }

    @AfterTest
    public void testCloseApplication(){
        Assert.assertTrue(baseClass.closeApplication());
    }
}
