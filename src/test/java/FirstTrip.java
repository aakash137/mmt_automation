public class FirstTrip {
    String price,duration,company,depTime,depTerminal,arrTime,arrTerminal;

    public String getPrice() {
        return price;
    }

    public String getDuration() {
        return duration;
    }

    public String getCompany() {
        return company;
    }

    public String getDepTime() {
        return depTime;
    }

    public String getDepTerminal() {
        return depTerminal;
    }

    public String getArrTime() {
        return arrTime;
    }

    public String getArrTerminal() {
        return arrTerminal;
    }

    public FirstTrip(String price, String duration, String company, String depTime, String depTerminal, String arrTime, String arrTerminal) {
        this.price = price;
        this.duration = duration;
        this.company = company;
        this.depTime = depTime;
        this.depTerminal = depTerminal;
        this.arrTime = arrTime;
        this.arrTerminal = arrTerminal;
    }
}
