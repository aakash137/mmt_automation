import com.miq.browser.Browser;
import com.miq.browser.BrowserType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class BaseClass {

    static WebDriver webDriver;
    String baseUrl = "https://www.goibibo.com/";
    String expectedTitle = "Online flight booking, Hotels, Bus &amp; Holiday Packages at Goibibo";
    String actualTitle = "";

    ArrayList<FirstTrip> firstTrips = new ArrayList<>();
    ArrayList<RoundTrip> roundTrips = new ArrayList<>();

    public void setupApplication() {

//        try {
//            System.setProperty("webdriver.chrome.driver", "/home/aakash.chauhan/Repos/Activation/mmtautobook/src/test/resources/drivers/chromedriver");
//
//            Reporter.log("=====Browser Session Started=====", true);
//            webDriver = new ChromeDriver();
//            webDriver.manage().window().maximize();
//            webDriver.get(baseUrl);
//
//            Reporter.log("=====Application Started=====", true);
//            return true;
//
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        return false;
        webDriver=Browser.getInstance(BrowserType.chrome,false);
        webDriver.get(baseUrl);

    }

   /* public void setupApplication() {

       //webDriver = Browser.getInstance(BrowserType.chrome,false);
    }*/


    public FirstFlightDetails journeyDetails(String src, String dest) {


        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //selects the source of flight
        WebElement from = webDriver.findElement(By.id("gosuggest_inputSrc"));
        from.click();
        from.sendKeys(src);
        webDriver.findElement(By.id("react-autosuggest-1-suggestion--0")).click();

        //selects the destination of flight

        WebElement to = webDriver.findElement(By.id("gosuggest_inputDest"));
        to.click();
        to.sendKeys(dest);
        webDriver.findElement(By.id("react-autosuggest-1-suggestion--0")).click();


        //selects current date
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());
        //  date = Arrays.toString(date.split("-"));
        date = String.join("", date.split("-"));
        webDriver.findElement(By.xpath("//*[@id=\"searchWidgetCommon\"]/div[1]/div[1]/div[1]/div/div[5]/input")).click();

        webDriver.findElement(By.id("fare_" + date)).click();


        //return date is set 2 days after
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 2);
        String returnDate = simpleDateFormat.format(calendar.getTime());
        returnDate = String.join("", returnDate.split("-"));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        webDriver.findElement(By.xpath("//*[@id=\"searchWidgetCommon\"]/div[1]/div[1]/div[1]/div/div[7]/input[1]")).click();
        webDriver.findElement(By.id("fare_" + returnDate)).click();
        webDriver.findElement(By.id("gi_search_btn")).click();

        String srCompany = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[1]/div[2]/div[2]/div[1]/span[2]")).getText();
        String depTime = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[1]/div[2]/div[2]/div[2]/div/div[1]/span[1]")).getText();
        String depTerminal = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[1]/div[2]/div[2]/div[2]/div/div[1]/span[2]")).getText();
        String srcDuration = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[1]/div[2]/div[2]/div[2]/div/div[2]/div")).getText();
        String arrivalTime = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[1]/div[2]/div[2]/div[2]/div/div[3]/span[1]")).getText();
        String arrivalTerminal = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[1]/div[2]/div[2]/div[2]/div/div[3]/span[2]")).getText();

        String price = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[3]/div/div[1]/span/span")).getText();

        System.out.println("Flight details for 1st round");
        System.out.println("Departure Details " + depTime + " from " + depTerminal);
        System.out.println("Arrival Details " + arrivalTime + " from " + arrivalTerminal + "\n\n");


      //  FirstTrip firstTrip = new FirstTrip(price,srcDuration,srCompany,depTime,depTerminal,arrivalTime,arrivalTerminal);

        String returnCompany = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[2]/div[2]/div[1]/span[2]")).getText();
        String retunrDepTime = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[2]/div[2]/div[2]/div/div[1]/span[1]")).getText();
        String returnDepTerminal = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[2]/div[2]/div[2]/div/div[1]/span[2]")).getText();
        String returnDuration = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[2]/div[2]/div[2]/div/div[2]/div")).getText();
        String returnArrTime = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[2]/div[2]/div[2]/div/div[3]/span[1]")).getText();
        String returnArrTerminal = webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[2]/div[2]/div[2]/div/div[3]/span[2]")).getText();

    //    RoundTrip roundTrip = new RoundTrip(price,returnDuration,returnCompany,retunrDepTime,returnDepTerminal,returnArrTime,returnArrTerminal);

        FirstFlightDetails firstFlightDetails = new FirstFlightDetails(price,depTime+ "  "+depTerminal,arrivalTime+"  "+arrivalTerminal,srCompany,returnCompany,retunrDepTime+"  "+returnDepTerminal,returnArrTime+"  "+returnArrTerminal);

        System.out.println("Flight details for Return");
        System.out.println("Departure Details " + retunrDepTime + " from " + returnDepTerminal);
        System.out.println("Arrival Details " + returnArrTime + " from " + returnArrTerminal + "\n\n");

        Reporter.log("Clicked");

        ////*[contains(@class,"Day--today")]
        return firstFlightDetails;


    }

    public boolean getFlightResults() {
        String price, duration, company, depTime, depTerminal, arrTime, arrTerminal;


        // for first journey

        try {
            for (int i = 3; i < 10; i++) {
                company = webDriver.findElement(By.xpath("//*[@id=\"onwFltContainer\"]/div[" + i + "]/div[2]/div[1]/span[2]")).getText();
                depTime = webDriver.findElement(By.xpath("//*[@id=\"onwFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[1]/span[1]")).getText();
                depTerminal = webDriver.findElement(By.xpath("//*[@id=\"onwFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[1]/span[2]")).getText();
                duration = webDriver.findElement(By.xpath("//*[@id=\"onwFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[2]/div")).getText();
                arrTime = webDriver.findElement(By.xpath("//*[@id=\"onwFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[3]/span[1]")).getText();
                arrTerminal = webDriver.findElement(By.xpath("//*[@id=\"onwFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[3]/span[2]")).getText();
                price = webDriver.findElement(By.xpath("//*[@id=\"onwFltContainer\"]/div[" + i + "]/div[2]/div[3]/div[1]/span/span")).getText();

                firstTrips.add(new FirstTrip(price, duration, company, depTime, depTerminal, arrTime, arrTerminal));

                System.out.println(company + ": " + depTime + ": " + depTerminal + ": " + duration + ": " + arrTime + ": " + arrTerminal + ": " + price);

                company = webDriver.findElement(By.xpath("//*[@id=\"retFltContainer\"]/div[" + i + "]/div[2]/div[1]/span[2]")).getText();
                depTime = webDriver.findElement(By.xpath("//*[@id=\"retFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[1]/span[1]")).getText();
                depTerminal = webDriver.findElement(By.xpath("//*[@id=\"retFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[1]/span[2]")).getText();
                duration = webDriver.findElement(By.xpath("//*[@id=\"retFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[2]/div")).getText();
                arrTime = webDriver.findElement(By.xpath("//*[@id=\"retFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[3]/span[1]")).getText();
                arrTerminal = webDriver.findElement(By.xpath("//*[@id=\"retFltContainer\"]/div[" + i + "]/div[2]/div[2]/div/div[3]/span[2]")).getText();
                price = webDriver.findElement(By.xpath("//*[@id=\"retFltContainer\"]/div[" + i + "]/div[2]/div[3]/div[1]/span/span")).getText();


                roundTrips.add(new RoundTrip(price, duration, company, depTime, depTerminal, arrTime, arrTerminal));
                System.out.println(company + ": " + depTime + ": " + depTerminal + ":  " + duration + ": " + arrTime + ": " + arrTerminal + ": " + price + "\n\n");
            }


            fillUserDetails();

            webDriver.findElement(By.xpath("//*[@id=\"travellerForm\"]/div[2]/button")).click();
            return true;
        }catch (Exception e){
            System.out.println(""+e.getMessage());
        }

        return false;

    }

    private synchronized void fillUserDetails() {

        webDriver.findElement(By.xpath("//*[@id=\"fltTcktVoucher\"]/div[3]/div/div[2]/input")).click();

        WebElement scrollTo = webDriver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div[2]/div/div[1]/div[4]/div"));

        scroll_Page(scrollTo, 100);

        //fill up the form
        Select prefix = new Select(webDriver.findElement(By.name("Adultchoose1")));
        prefix.selectByVisibleText("Mr");

        webDriver.findElement(By.id("AdultfirstName1")).sendKeys("Aakash");
        webDriver.findElement(By.id("AdultlastName1")).sendKeys("Chauhan");

      //  new Select(webDriver.findElement(By.name("Adultdob_year1"))).selectByVisibleText("2001");
        webDriver.findElement(By.name("email")).sendKeys("aakash.chauhan@miqdigital.com");
        webDriver.findElement(By.name("mobile")).sendKeys("9779487776");

      //  webDriver.findElement(By.xpath("//*[@id=\"travellerForm\"]/div[2]/button")).click();


    }

    public synchronized boolean scroll_Page(WebElement webelement, int scrollPoints) {
        try {
            Actions dragger = new Actions(webDriver);
            // drag downwards
            int numberOfPixelsToDragTheScrollbarDown = 10;
            for (int i = 10; i < scrollPoints; i = i + numberOfPixelsToDragTheScrollbarDown) {
                dragger.moveToElement(webelement).clickAndHold().moveByOffset(0, numberOfPixelsToDragTheScrollbarDown).release(webelement).build().perform();
            }
            Thread.sleep(500);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean closeApplication() {
        try {
            webDriver.close();
            Reporter.log("=====Application Closed=====", true);
            return true;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return false;


    }

}
