public class FirstFlightDetails {


    public String getPrice() {
        return price;
    }

    public String getDepTimeTerminal() {
        return depTimeTerminal;
    }

    public String getArrTimeTerminal() {
        return arrTimeTerminal;
    }

    public String getSrcFlight() {
        return srcFlight;
    }

    public String getRetrnFlight() {
        return retrnFlight;
    }

    public FirstFlightDetails(String price, String depTimeTerminal, String arrTimeTerminal, String srcFlight, String retrnFlight, String returndDeptTimeTerminal, String retrnArrTimeTerminal) {
        this.price = price;
        this.depTimeTerminal = depTimeTerminal;
        this.arrTimeTerminal = arrTimeTerminal;
        this.srcFlight = srcFlight;
        this.retrnFlight = retrnFlight;
        this.returndDeptTimeTerminal = returndDeptTimeTerminal;
        this.retrnArrTimeTerminal = retrnArrTimeTerminal;
    }

    String price;
    String depTimeTerminal;
    String arrTimeTerminal;
    String srcFlight;
    String retrnFlight;

    String returndDeptTimeTerminal;

    String retrnArrTimeTerminal;

    public String getReturndDeptTimeTerminal() {
        return returndDeptTimeTerminal;
    }



    public String getRetrnArrTimeTerminal() {
        return retrnArrTimeTerminal;
    }






}
